BEGIN TRANSACTION;
CREATE TABLE user (
	id INTEGER NOT NULL, 
	username VARCHAR(20), 
	email VARCHAR(80), 
	password VARCHAR(80), 
	role BOOLEAN, 
	PRIMARY KEY (id), 
	UNIQUE (username), 
	UNIQUE (email), 
	CHECK (role IN (0, 1))
);
INSERT INTO `user` VALUES (1,'admin','admin@admin.com','sha256$ePpRGCoA$62be853fa8c9e230fc407212ba9885f7a6de6b7fa5fe8902ad30cfdd053d0613',1);
CREATE TABLE relation (
	quiz_id INTEGER, 
	problem_id INTEGER, 
	FOREIGN KEY(quiz_id) REFERENCES quizes (id), 
	FOREIGN KEY(problem_id) REFERENCES problems (id)
);
INSERT INTO `relation` VALUES (1,2);
INSERT INTO `relation` VALUES (1,3);
INSERT INTO `relation` VALUES (1,5);
INSERT INTO `relation` VALUES (1,7);
INSERT INTO `relation` VALUES (1,8);
INSERT INTO `relation` VALUES (2,1);
INSERT INTO `relation` VALUES (2,3);
INSERT INTO `relation` VALUES (2,5);
INSERT INTO `relation` VALUES (2,7);
CREATE TABLE quizes (
	id INTEGER NOT NULL, 
	name VARCHAR(100), 
	PRIMARY KEY (id)
);
INSERT INTO `quizes` VALUES (1,'Quiz1');
INSERT INTO `quizes` VALUES (2,'Quiz2');
INSERT INTO `quizes` VALUES (3,'Quiz3');
INSERT INTO `quizes` VALUES (4,'Quiz4');
CREATE TABLE problems (
	id INTEGER NOT NULL, 
	statement VARCHAR(100), 
	option_first VARCHAR(100), 
	option_second VARCHAR(100), 
	option_third VARCHAR(100), 
	option_fourth VARCHAR(100), 
	answer_first BOOLEAN, 
	answer_second BOOLEAN, 
	answer_third BOOLEAN, 
	answer_fourth BOOLEAN, 
	type_of_ques BOOLEAN, 
	attempted BOOLEAN, 
	PRIMARY KEY (id), 
	UNIQUE (statement), 
	CHECK (answer_first IN (0, 1)), 
	CHECK (answer_second IN (0, 1)), 
	CHECK (answer_third IN (0, 1)), 
	CHECK (answer_fourth IN (0, 1)), 
	CHECK (type_of_ques IN (0, 1)), 
	CHECK (attempted IN (0, 1))
);
INSERT INTO `problems` VALUES (1,'Test1','1','2','3','4',1,0,0,0,1,0);
INSERT INTO `problems` VALUES (2,'Test2','1','2','3','4',0,1,0,0,0,0);
INSERT INTO `problems` VALUES (3,'Test4','1','2','3','4',0,1,0,0,0,0);
INSERT INTO `problems` VALUES (4,'Test3','1','2','3','4',0,1,1,0,1,0);
INSERT INTO `problems` VALUES (5,'Test5','1','2','3','4',0,1,1,0,1,0);
INSERT INTO `problems` VALUES (6,'Test6','1','2','3','4',0,1,1,0,1,0);
INSERT INTO `problems` VALUES (7,'Test7','1','2','3','4',0,1,1,0,1,0);
INSERT INTO `problems` VALUES (8,'Test9','1','2','3','4',0,1,1,0,1,0);
COMMIT;
